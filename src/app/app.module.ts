import { NgModule } from '@angular/core';
import { AppComponent } from './app.component'
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
   imports: [
      CommonModule,
      BrowserModule ],
   declarations: [
      AppComponent
   ],
   providers: [],
   bootstrap: [AppComponent]
})

export class AppModule { }