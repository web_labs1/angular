import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { Animal } from './animal'

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'my-first-project';
  show_cats_flag = false;
  animals = [
    {"type": "cat",
    "name": "vasya",
    "legs_count": 4},
    {"type": "bird",
    "name": "biba",
    "legs_count": 2},
    {"type": "dog",
    "name": "boba",
    "legs_count": 4},
    {"type": "cat",
    "name": "fedya",
    "legs_count": 4}]
  all_animals: Animal[] = []
  cats: Animal[] = []
  constructor() {}

  init() {
    this.all_animals = []
    this.animals.forEach(element => {
      this.all_animals.push(new Animal(element.name, element.type, element.legs_count));
    });
    console.log(this.all_animals);
  }

  onClick(name:String) {
   this.all_animals.forEach(item => {
    if (item.name == name) {
      alert(`addition info: legs count = ${item.legs_count}`)
    }
   })
  }

  show_cats() {
    this.all_animals = []
    if (!this.show_cats_flag) {
      this.animals.forEach(item => {
        if (item.type == 'cat') {
          this.all_animals.push(new Animal(item.name, item.type, item.legs_count));
        }
      })
      this.show_cats_flag = true
    } else {
      this.hide_cats()
    }
    
  }

  hide_cats() {
    this.all_animals = []
    this.animals.forEach(element => {
      this.all_animals.push(new Animal(element.name, element.type, element.legs_count));
    });
    console.log(this.all_animals);
    this.show_cats_flag = false
  }
}
