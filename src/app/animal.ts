export class Animal {
    name: String;
    type: String;
    legs_count: Number;
    constructor(name: String, type: String, legs_count: Number) {
      this.name = name;
      this.type = type;
      this.legs_count = legs_count;
    }
  }
